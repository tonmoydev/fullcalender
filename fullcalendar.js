var calenderObject ;
var Calendar = {};
var fullCalendar = function(element) {
	this.calenderHolder = document.querySelector(element);
	this.date = this.getDateObj();
	
	calenderObject = this;
}
fullCalendar.prototype.getOptions = function() {
	if ( this.options.display.weekday == undefined ) {
		this.options.display.weekday = 's' // s = short , l = long
	}

	if ( this.options.display.view == undefined ) {
		this.options.display.view = 'month' // month, day, week 
	}
} 
fullCalendar.prototype.getEvents = function() {
	if ( this.events == undefined ) {
		this.events = [];
	}
} 
fullCalendar.prototype.weekDays = {
	0 : { dayNo : 0, shortName : 'Sun', longName : 'Sunday' },
	1 : { dayNo : 1, shortName : 'Mon', longName : 'Monday' },
	2 : { dayNo : 2, shortName : 'Tue', longName : 'Tuesday' },
	3 : { dayNo : 3, shortName : 'Wed', longName : 'Wednesday' },
	4 : { dayNo : 4, shortName : 'Thu', longName : 'Thursday' },
	5 : { dayNo : 5, shortName : 'Fri', longName : 'Friday' },
	6 : { dayNo : 6, shortName : 'Sat', longName : 'Saturday' },
};
fullCalendar.prototype.getNumWeeksForMonth = function(year,month){
	date = new Date(year,month-1,1);
	day = date.getDay();
	numDaysInMonth = new Date(year, month, 0).getDate();
	return Math.ceil((numDaysInMonth + day) / 7);
}
fullCalendar.prototype.getDateObj = function(date) {
	var C 			= {};
	if (date == undefined) {
		C.date 		= moment();	
	} else {
		C.date 		= moment(date);
	}
	
	C.month 		= C.date.format("MM");
	C.year 			= C.date.format("YYYY");
	C.day 			= C.date.format("DD");
	C.weekDay 		= C.date.day();
	C.week 			= C.date.week();
	C.dayInMonth 	= C.date.daysInMonth();
	C.totalWeekOfMonth = this.getNumWeeksForMonth(C.year, C.month);
	return C;
}
fullCalendar.prototype.createElement = function(element) {
	return document.createElement(element);
}
fullCalendar.prototype.buildControl = function() {
	this.getOptions();
	var controlContainer = this.createElement("div");

	var btnContainer = this.createElement("div");
	btnContainer.classList.add("btn-container");

	

	var prevBtn = this.createElement("button");
	prevBtn.innerHTML = "&lt;"
	prevBtn.setAttribute("id","prev-control-btn");
	prevBtn.setAttribute("title","Previous");
	prevBtn.classList.add("control-btn");
	btnContainer.appendChild(prevBtn);

	var nextBtn = this.createElement("button");
	nextBtn.innerHTML = '&gt;';
	nextBtn.setAttribute("id","next-control-btn");
	nextBtn.setAttribute("title","Next");
	nextBtn.classList.add("control-btn");
	btnContainer.appendChild(nextBtn);

	controlContainer.appendChild(btnContainer);	

	var captionElement = this.createElement("div");
	captionElement.classList.add("caption-container");
	if (this.options.display.view == 'month') {
		captionElement.appendChild( document.createTextNode( this.date.date.format("MMMM YYYY") ) );
	}
	controlContainer.appendChild(captionElement);		


	controlContainer.classList.add("control-container");

	var tableContainer = this.createElement("div");
	tableContainer.classList.add("full-calender-container");
	tableContainer.appendChild(controlContainer);

	this.calenderHolder.appendChild(tableContainer);

	document.getElementById("next-control-btn").addEventListener("click", this.buildNextBlock);
	document.getElementById("prev-control-btn").addEventListener("click", this.buildPrevBlock);
}
fullCalendar.prototype.buildNextBlock = function() {
	calenderObject.buildNextPrevBlock('next');	
}
fullCalendar.prototype.buildPrevBlock = function() {
	calenderObject.buildNextPrevBlock('prev');	
}
fullCalendar.prototype.buildNextPrevBlock = function(type) {

	var date = calenderObject.date.date;
	if (calenderObject.options.display.view == 'month') {
		if (type == 'next'){
			date = date.add(1, "months").startOf("month");
		} else if (type == 'prev'){
			date = date.add(-1, "months").startOf("month");
		}
		calenderObject.date = calenderObject.getDateObj( date );
		var tableContainer = document.querySelector('.full-calender-container');
		var table = document.querySelector('.full-calander');
		tableContainer.removeChild(table);

		if (calenderObject.options.display.view == 'month') {
			document.querySelector(".caption-container").innerText = calenderObject.date.date.format("MMMM YYYY")
		}
		calenderObject.build();
	}
}

fullCalendar.prototype.build = function() {
	this.getOptions();
	var C = this.date;
	var calanderTable = this.createElement("table");
	calanderTable.setAttribute("cellpadding",0);
	calanderTable.setAttribute("cellspacing",0);
	calanderTable.classList.add("full-calander");

	if (this.options.display.view == 'month') {
		var thead = this.createElement("thead");
		var theadTr = this.createElement("tr");
		var th, tMode;
		for(var d in this.weekDays) {
			var day = this.weekDays[d];
			th = this.createElement("th");
			switch(this.options.display.weekday) {
				case "s" :
					tMode = document.createTextNode( day.shortName );
					break;
				case "l" :
					tMode = document.createTextNode( day.longName )
					break;
			}
			th.appendChild(tMode);
			theadTr.appendChild(th);
		}
		thead.appendChild(theadTr);
		calanderTable.appendChild(thead);

		var tbody = this.createElement("tbody");
		var tbodyTr, td,  tMode, eventElement ;
		var tdDate = 1; 

		for (var w = 0; w < C.totalWeekOfMonth; w++) {

			tbodyTr = this.createElement("tr");
			for(var d in this.weekDays) {
				td = this.createElement("td");
				var date = moment(C.year+"-"+C.month+"-"+tdDate);
				if (tdDate <= C.dayInMonth && date.day() == this.weekDays[d].dayNo) {
					tMode = document.createTextNode( tdDate);
					tdDate ++;
					td.appendChild(tMode);
					if (this.events != undefined) {
						var dayEvent = this.events.filter(function (e) { return e.date == date.format('YYYY-MM-DD') });
						if (dayEvent.length > 0) {
							var eventContainerElement = document.createElement("div");
							eventContainerElement.classList.add("event-container");
							for( var eventIndex in dayEvent ) {
								eventElement = document.createElement("div");
								eventElement.classList.add("event-item");
								if (eventIndex < 4) {
									eventElement.innerText = ( dayEvent[eventIndex].title.length > 20 ) ? dayEvent[eventIndex].title.substring(0,20)+' ...' : dayEvent[eventIndex].title;
									
									if ( dayEvent[eventIndex].style != undefined ) {
										var style = "";
										if(dayEvent[eventIndex].style.fontcolor != undefined ) {
											style = "color : "+dayEvent[eventIndex].style.fontcolor+"; ";
										}
										if(dayEvent[eventIndex].style.backcolor != undefined ) {
											style += "background-color : "+dayEvent[eventIndex].style.backcolor+";";
										}
										eventElement.setAttribute("style", style);
									}	
									eventContainerElement.appendChild(eventElement);
								} else {
									eventElement.classList.add("event-more");
									eventElement.innerText = "more "+ (dayEvent.length - eventIndex) +" events...";
									eventElement.setAttribute("data-event-date", date.format('YYYY-MM-DD'));
									eventElement.addEventListener("click", this.getMoreEvents);
									eventContainerElement.appendChild(eventElement);
									break;
								}
								
								

							}
							td.appendChild(eventContainerElement);
						}
					}
					td.classList.add("active");
				} else {
					td.classList.add("disabled");
				}
				tbodyTr.appendChild(td);
				
			}
			tbody.appendChild(tbodyTr);

		}
		
		calanderTable.appendChild(tbody);
	}
	var tableContainer = document.querySelector('.full-calender-container');
	tableContainer.appendChild(calanderTable)
	this.calenderHolder.appendChild(tableContainer);

}
fullCalendar.prototype.closeModal = function() {
	var tableContainer = document.querySelector('.full-calender-container');
	tableContainer.removeChild( document.querySelector(".modal") );
}
fullCalendar.prototype.getMoreEvents = function() {
	var clickElement = this;
	var eventDate = clickElement.getAttribute("data-event-date");
	eventDate = moment(eventDate).format('YYYY-MM-DD');
	var dayEvent = calenderObject.events.filter(function (e) { return e.date == eventDate });
	
	var tableContainer = document.querySelector('.full-calender-container');

	if (dayEvent.length > 0) {
		var eventContainerElement = document.createElement("div");
		if (document.querySelector(".modal") != undefined){ 
			tableContainer.removeChild( document.querySelector(".modal") );
		}
		var modal = document.createElement("div");
		modal.classList.add("modal");

		var modalContainer = document.createElement("div");
		modalContainer.classList.add("modal-container");

		var modalHead = document.createElement("div");
		modalHead.classList.add("modal-head");
		var modalHeadTextElement = document.createElement("h4");
		modalHeadTextElement.innerText = 'Events of  '+moment(eventDate).format("Do MMMM YYYY");
		modalHead.appendChild(modalHeadTextElement);

		var modalHeadCrossElement = document.createElement("span");
		modalHeadCrossElement.innerHTML = '&#10006;';
		modalHeadCrossElement.addEventListener("click", calenderObject.closeModal);
		modalHead.appendChild(modalHeadCrossElement);
		

		modalContainer.appendChild(modalHead);

		var modalBody = document.createElement("div");
		modalBody.classList.add("modal-body");	


		eventContainerElement.classList.add("event-container");
		for( var eventIndex in dayEvent ) {
			eventElement = document.createElement("div");
			eventElement.classList.add("event-item");
			
			eventElement.innerText = dayEvent[eventIndex].title;
			
			if ( dayEvent[eventIndex].style != undefined ) {
				var style = "";
				if(dayEvent[eventIndex].style.fontcolor != undefined ) {
					style = "color : "+dayEvent[eventIndex].style.fontcolor+"; ";
				}
				if(dayEvent[eventIndex].style.backcolor != undefined ) {
					style += "background-color : "+dayEvent[eventIndex].style.backcolor+";";
				}
				eventElement.setAttribute("style", style);
			}	
			eventContainerElement.appendChild(eventElement);
		
		}
		modalBody.appendChild(eventContainerElement);
		if (dayEvent.length > 10) {
			modalBody.setAttribute("style"," height: 200px;overflow-y: scroll");
		} else {
			modalBody.setAttribute("style","");
		}
		modalContainer.appendChild(modalBody);
		modal.appendChild(modalContainer);
		tableContainer.appendChild(modal);
	}

	

}


Calendar.obj = {};
Calendar.render = function(element, options, events) {
	if (element != undefined){
		Calendar.obj = new fullCalendar(element);
		if (options != undefined){
			Calendar.obj.options = options;
		}
		if(events != undefined){
			Calendar.obj.events = events;
		}
		Calendar.obj.buildControl();
		Calendar.obj.build();
	}
}
Calendar.setEvents = function(events) {

	if(events != undefined){
		Calendar.obj.events = events;
		var tableContainer = document.querySelector('.full-calender-container');
		var table = document.querySelector('.full-calander');
		tableContainer.removeChild(table);
		Calendar.obj.build();
	}
}
