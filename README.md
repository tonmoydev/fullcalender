# Full Calendar #



### What is this repository for? ###

This is a javascript plugin for implementing full calendar with multiple events

### How do I get set up? ###

* need to include files
#### Script ####
	<script type="text/javascript" src="moment.min.js"></script>
	<script type="text/javascript" src="fullcalendar.js"></script>

#### Style ####
	<link rel="stylesheet" type="text/css" href="fullcalendar.css">

* Configuration

### HTML ###

	<div class="calendar"></div>

	Note : class "calendar" you can change as per your requirement

### SCRIPT ###
	<script type="text/javascript">
		var options = {
			display : {
				weekday : 'l',
			}
		};
		Calendar.render(".calendar", options);
	</script>

	Note: display >> weekday  = there are 2 options. Like 'l' = 'Sunday / Monday', 's' = 'SUN / MON' 
	
	To set events with the calendar

	<script>
		var events = [
		    {
		        "date": "2017-12-31",
		        "title": "Happy Birthday Devid",
		        "style": {
		            "fontcolor": "#efe3e3",
		            "backcolor": "#1a42bc"
		        }
		    },
		    {
		        "date": "2017-12-31",
		        "title": "Event 1",
		        "style": {
		            "fontcolor": "#efe3e3",
		        }
		    },
		    {
		        "date": "2018-01-20",
		        "title": "Event 2"
		    },
		    {
		        "date": "2017-12-10",
		        "title": "Event 3"
		    },
		    {
		        "date": "2017-12-12",
		        "title": "Event 4"
		    }
	    ];
	    Calendar.setEvents(events);
	</script>

	Note : to include events from remote data source just implement the ajax.

	<script>
		$.ajax({
			url : "data.json",
			type : 'GET',
			dataType : 'JSON',
			success : function(events) {
				Calendar.setEvents(events);
			}
		})
	</script>

	Note : for reference you can take a look on folder "demo"